# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Tutorial de Uso: 1- Tenha na sua máquina o Java 1.8 instalado.

2- Tenha uma IDE Java instalado em sua máquina (Netbeans ou Eclipse).

3- Na IDE selecione o projeto para abrir.

4- Clique no botão para executar o projeto.

5- Com o programa em execução entre como empregado: Usuário: admin Senha: admin.

6- Abrirá a tela de Pedidos, onde vocé podera cadrastar produtos e criar um novo pedido, alem de cadastrar um cliente.

7- Apos adicionar um produto ao stock vocẽ podera adicionar ele ao carrinho do cliente simplesmente clicando no botão "add cart".

8- Para finalizar um pedido clique em "proced" e logo depois em pay.